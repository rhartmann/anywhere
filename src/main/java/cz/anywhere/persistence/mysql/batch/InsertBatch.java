package cz.anywhere.persistence.mysql.batch;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

/**
 * @author Radim Hartmann
 * 
 * row number	column number	time [s]	batch size
 * 1 000 000	26				36			 5 000
 * 1 000 000	 5				16			20 000
 * 
 */

public class InsertBatch {
	private final Logger log = Logger.getLogger(InsertBatch.class);

	private static final String CONFIG_PATH = "classpath:application-config.xml";

	private static final String COLUMN_VALUE = "asdfasdfdadlkjnlkhul";
	private static final int ROW_COUNT  = 1000000;
	private static final int BATCH_SIZE = 5000;
	private static final int COLUMN_COUNT = 26; //MAX 26 
	
	private DataSourceTransactionManager driverManagerDataSource;

	public static void main(String[] args) throws SQLException {
		InsertBatch ib = new InsertBatch();
		ib.execute();
	}

	private void execute() throws SQLException {
		initSpringContext();
		eraseOldData();
		final List<String[]> list = populateList();
		insetData(list);
	}

	private void initSpringContext() throws SQLException {
		@SuppressWarnings("resource")
		final ApplicationContext context = new ClassPathXmlApplicationContext(CONFIG_PATH);
		driverManagerDataSource = (DataSourceTransactionManager)context.getBean("transactionManager");
	}

	private void eraseOldData() throws SQLException {
		long start = System.nanoTime();
		driverManagerDataSource.getDataSource().getConnection().createStatement().execute("delete from test");
		log.debug("Erase test table "+ (System.nanoTime() - start)/1000000 + " ms.");
	}

	private List<String[]> populateList() {
		long start = System.nanoTime();
		final List<String[]> list = new ArrayList<>(ROW_COUNT);
		
		String[] strr;
		for (int i = 1; i <= ROW_COUNT; i++) {
			strr = new String[COLUMN_COUNT];
			strr[0] = "" + i;
			
			for (int columnIdx = 1; columnIdx <= COLUMN_COUNT-1; columnIdx++) {
				strr[columnIdx] = COLUMN_VALUE;
			}
			
			list.add(strr);
		}
		log.debug("Populate list finished in "+ (System.nanoTime() - start)/1000000 + " ms.");
		return list;
	}
		
	private void insetData(final List<String[]> list) throws SQLException {
		long start = System.nanoTime();
		
		String sql = "INSERT INTO Test (ID," + getColumnNames() + ") VALUES "; 
		
		StringBuilder sb = new StringBuilder(sql);
		
		Connection connection = driverManagerDataSource.getDataSource().getConnection();
		connection.setTransactionIsolation(Connection.TRANSACTION_REPEATABLE_READ);
		connection.setAutoCommit(false);

		for (int i = 0; i < list.size(); i++) {
			String[] str = list.get(i);
			sb.append("(")
			.append(str[0]).append(", ");
			
			for (int columnIdx = 1; columnIdx <= COLUMN_COUNT-2; columnIdx++) {
				sb.append("'").append(str[columnIdx]).append("',");
			}
			sb.append("'").append(str[COLUMN_COUNT-1]).append("'),");
			
			if (i % BATCH_SIZE == 0) {
                log.debug(i);
                executeStatement(connection, sb);
                sb = new StringBuilder(sql);
            }
		}
		executeStatement(connection, sb);
        connection.close();

        log.debug("Insert list finished in "+ (System.nanoTime() - start)/1000000 + " ms.");	
	}
	
	private void executeStatement(Connection connection, StringBuilder sb) throws SQLException {
        String s = sb.substring(0, sb.length()-1);
        connection.createStatement().execute(s);
        connection.commit();
	}

	private String getColumnNames(){
		StringBuffer sb = new StringBuffer();
		for (int i = 1; i < COLUMN_COUNT; i++) {
			sb.append("text_").append(i).append(",");
		}
		return sb.substring(0, sb.length()-1);
	}
}